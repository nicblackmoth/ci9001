from fib import fib

def test_fib():
    assert fib(3) == 2
    assert fib(9) == 34
    assert fib(20) == 6765

def test_fib_base_case_0():
    assert fib(0) == 0

def test_fib_base_case_1():
    assert fib(1) == 1

def test_fib_inductive_case_1():
    assert fib(2) == 1