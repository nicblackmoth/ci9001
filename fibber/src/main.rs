use cached::proc_macro::cached;

fn main() {
    println!("Hello, world!");
}

#[cached]
fn fib(x: u64) -> u64 {
    if x == 0 || x == 1 {
        return x;
    }
    return fib(x - 2) + fib(x - 1);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn basic_fib_usage() {
        assert_eq!(fib(3), 2);
        assert_eq!(fib(9), 34);
        assert_eq!(fib(20), 6765);
        assert_eq!(fib(21), 10946);
    }
}