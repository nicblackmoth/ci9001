from functools import lru_cache

@lru_cache(maxsize=None)
def fib(x: int) -> int:
    # Base case 1:
    if x == 0:
        return 0
    # Base case 2"::
    if x == 1:
        return 1
    # Inductive case:
    return fib(x - 2) + fib(x - 1)
